@extends('layouts.app')

@section('content')
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                List of Tags
            </div>
            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                        <th>Id</th>
                        <th>Tags</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </thead>
                    <tbody>
                    @if ($tags->count() > 0)
                        @foreach ($tags as $tag)
                            <tr>
                                <td> {{ $tag->id }} </td>
                                <td> {{ $tag->tag }} </td>
                                <td>
                                    <a href="{{ route('tag.edit', ['id' => $tag->id])}}" class="btn btn-xs btn-info">
                                        Edit
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('tag.delete', ['id' => $tag->id])}}" class="btn btn-xs btn-danger">
                                        Delete
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                            <tr>
                                <th colspan = "4" class = "text-center">No tags</th>
                            </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection