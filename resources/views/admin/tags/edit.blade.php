@extends('layouts.app')

@section('content')
<div class="col-md-8">
    <div class="card">
        <div class="card-header">
            Update Tags {{$tag->name}}
        </div>
        <div class="card-body">
            <form action="{{ route('tag.update', ['id' => $tag->id ])}}" method="post" enctype= "multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="tag">Name</label>
                <input type="text" name = "tag" class="form-control {{$errors->has('tag') ? 'is-invalid' : ''}}" value="{{$tag->tag}}">
                </div>
                <div class="form-group">
                    <div class="text-center">
                        <button class="btn btn-success" type="submit">Update tag</button>
                    </div>
                </div>
               @include('admin.includes.errors')
            </form>
        </div>
    </div>
</div>

@endsection

