@extends('layouts.app')

@section('content')
<div class="col-md-8">
    <div class="card">
        <div class="card-header">
            Create a new tag
        </div>
        <div class="card-body">
            <form action="{{ route('tag.store')}}" method="post" enctype= "multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="tag">Tag</label>
                <input type="text" name = "tag" class="form-control {{$errors->has('tag') ? 'is-invalid' : ''}}">
                </div>
                <div class="form-group">
                    <div class="text-center">
                        <button class="btn btn-success" type="submit">Store tag</button>
                    </div>
                </div>
               @include('admin.includes.errors')
            </form>
        </div>
    </div>
</div>

@endsection

