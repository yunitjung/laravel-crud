@extends('layouts.app')

@section('content')
<div class="col-md-8">
    <div class="card">
        <div class="card-header">
            Update Post {{$post->name}}
        </div>
        <div class="card-body">
            <form action="{{ route('post.update', ['id' => $post->id ])}}" method="post" enctype= "multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="name">Title</label>
                    <input type="text" name = "title" class="form-control {{$errors->has('title') ? 'is-invalid' : ''}}" value="{{$post->title}}">
                </div>
                <div class="form-group">
                    <label for="featured">Featured Image</label>
                    <input type="file" name = "featured" class="form-control {{$errors->has('featured') ? 'is-invalid' : ''}}">
                </div>
                <div class="form-group">
                    <label for="category">Category</label>
                    <select name="category_id" id="category_id" class = "form-control">
                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}"
                                @if ($post->category_id == $category->id)
                                    selected
                                @endif>{{ $category->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                        <label> Tags</label>
                        @foreach ($tags as $tag)
                            <div class="form-check">
                                <input type="checkbox" value = "{{$tag->id}}" name="tags[]" class = "form-check-input"
                                @foreach ($post->tags as $t)
                                    @if ($tag->id == $t->id)
                                        checked
                                    @endif
                                @endforeach
                                >
                                <label for="tags[]"  class = "form-check-label">{{$tag->tag}}</label>
                            </div>
                        @endforeach
                    </div>
                <div class="form-group">
                    <label for="content">Content</label>
                <textarea name = "content" id = "content" cols = "5" rows = "5" class="form-control {{$errors->has('content') ? 'is-invalid' : ''}}">{{$post->content}}</textarea>
                </div>
                <div class="form-group">
                    <div class="text-center">
                        <button class="btn btn-success" type="submit">Update post</button>
                    </div>
                </div>
               @include('admin.includes.errors')
            </form>
        </div>
    </div>
</div>

@endsection

