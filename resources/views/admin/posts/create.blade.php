@extends('layouts.app')

@section('content')
<div class="col-md-8">
    <div class="card">
        <div class="card-header">
            Create a new post
        </div>
        <div class="card-body">
            <form action="{{ route('post.store')}}" method="post" enctype= "multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="title">Title</label>
                <input type="text" name = "title" class="form-control {{$errors->has('title') ? 'is-invalid' : ''}}">
                </div>
                <div class="form-group">
                    <label for="featured">Featured Image</label>
                    <input type="file" name = "featured" class="form-control {{$errors->has('featured') ? 'is-invalid' : ''}}">
                </div>
                <div class="form-group">
                    <label for="category">Category</label>
                    <select name="category_id" id="category_id" class = "form-control">
                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label> Tags</label>
                    @foreach ($tags as $tag)
                        <div class="form-check">
                            <input type="checkbox" value = "{{$tag->id}}" name="tags[]" class = "form-check-input">
                            <label for="tags[]"  class = "form-check-label">{{$tag->tag}}</label>
                        </div>
                    @endforeach
                </div>
                <div class="form-group">
                    <label for="content">Content</label>
                    <textarea name = "content" id = "content" cols = "5" rows = "5" class="form-control {{$errors->has('content') ? 'is-invalid' : ''}}"></textarea>
                </div>
                <div class="form-group">
                    <div class="text-center">
                        <button class="btn btn-success" type="submit">Store post</button>
                    </div>
                </div>
                @if ($errors->any())

                    <ul class="list-group">
                        @foreach ($errors->all() as $error)
                            <li class="list-group-item list-group-item-danger">
                                {{ $error }}
                            </li>
                        @endforeach
                    </ul>

                @endif
            </form>
        </div>
    </div>
</div>

@endsection

