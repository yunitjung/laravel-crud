@extends('layouts.app')

@section('content')
<div class="col-md-8">
    <div class="card">
        <div class="card-header">
            Update Category {{$category->name}}
        </div>
        <div class="card-body">
            <form action="{{ route('category.update', ['id' => $category->id ])}}" method="post" enctype= "multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="name">Name</label>
                <input type="text" name = "name" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}" value="{{$category->name}}">
                </div>
                <div class="form-group">
                    <div class="text-center">
                        <button class="btn btn-success" type="submit">Update category</button>
                    </div>
                </div>
               @include('admin.includes.errors')
            </form>
        </div>
    </div>
</div>

@endsection

