@extends('layouts.app')

@section('content')
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                List of Category
            </div>
            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                        <th>Id</th>
                        <th>Category Name</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </thead>
                    <tbody>
                    @if ($categories->count() > 0)
                        @foreach ($categories as $category)
                            <tr>
                                <td> {{ $category->id }} </td>
                                <td> {{ $category->name }} </td>
                                <td>
                                    <a href="{{ route('category.edit', ['id' => $category->id])}}" class="btn btn-xs btn-info">
                                        Edit
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('category.delete', ['id' => $category->id])}}" class="btn btn-xs btn-danger">
                                        Delete
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                            <tr>
                                <th colspan = "4" class = "text-center">No Categories</th>
                            </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection