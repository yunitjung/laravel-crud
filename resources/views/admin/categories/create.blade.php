@extends('layouts.app')

@section('content')
<div class="col-md-8">
    <div class="card">
        <div class="card-header">
            Create a new category
        </div>
        <div class="card-body">
            <form action="{{ route('category.store')}}" method="post" enctype= "multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="name">Name</label>
                <input type="text" name = "name" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}">
                </div>
                <div class="form-group">
                    <div class="text-center">
                        <button class="btn btn-success" type="submit">Store category</button>
                    </div>
                </div>
               @include('admin.includes.errors')
            </form>
        </div>
    </div>
</div>

@endsection

