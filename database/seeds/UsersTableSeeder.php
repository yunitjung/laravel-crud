<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = App\User::create([
            'name' => 'Yuni',
            'email' => 'yuni@krafthaus.co.id',
            'password' => bcrypt('letmein!'),
            'admin' => 1
        ]);

        App\Profile::create([
            'user_id' => $user->id,
            'avatar' => 'uploads/avatars/1543299214DSC_0392.jpg',
            'about' => 'Lorem ipsum dolor sit amet,Stet clita kasd gubergren,  At vero eos et accusam et justo duo dolores et ea rebum.',
            'facebook' => 'facebook.com',
            'youtube' => 'youtube.com'
        ]);
    }
}
